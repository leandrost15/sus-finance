pragma solidity ^0.6.12;
// SPDX-License-Identifier: Unlicensed

interface IERC20 {
    function totalSupply() external view returns (uint256);
    function balanceOf(address account) external view returns (uint256);
    function transfer(address recipient, uint256 amount) external returns (bool);
    function allowance(address owner, address spender) external view returns (uint256);
    function approve(address spender, uint256 amount) external returns (bool);
    function transferFrom(address sender, address recipient, uint256 amount) external returns (bool);
}

/**
 * Se faz necessario em circunstancias de segurança:
 * 
 * O solidity por sua vez, é inseguro em questões matematicas. Isso porque é vulneravel á overflow.
 * Uma vez que o valor maximo de um tipo é estrapolado, o valor da variavel é resetado e o mesmo se aplica no outro extremo: Se o menor valor(0) for subtraido por um valor maior que 0, o valor da variavel se torna o maior valor que esse tipo pode conter.
 * Logo, a biblioteca SafeMath que é um padrão, evita que esse tipo de coisa aconteça
 * 
 **/
library SafeMath {
    function add(uint256 a, uint256 b) internal pure returns (uint256) {
        uint256 c = a + b;
        require(c >= a, "SafeMath: addition overflow");

        return c;
    }

    function sub(uint256 a, uint256 b) internal pure returns (uint256) {
        return sub(a, b, "SafeMath: subtraction overflow");
    }

    function sub(uint256 a, uint256 b, string memory errorMessage) internal pure returns (uint256) {
        require(b <= a, errorMessage);
        uint256 c = a - b;

        return c;
    }

    function mul(uint256 a, uint256 b) internal pure returns (uint256) {
        if (a == 0) {
            return 0;
        }

        uint256 c = a * b;
        require(c / a == b, "SafeMath: multiplication overflow");

        return c;
    }

    function div(uint256 a, uint256 b) internal pure returns (uint256) {
        return div(a, b, "SafeMath: division by zero");
    }

    function div(uint256 a, uint256 b, string memory errorMessage) internal pure returns (uint256) {
        require(b > 0, errorMessage);
        uint256 c = a / b;

        return c;
    }
}

contract SUSmath {
    using SafeMath for uint256;
    
    /**
    * Por solidity não aceitar valores reais, foi necessario criar uma equação para obter a porcentagem quebrada de um valor:
    * Respectivamente, é atribuido o valor da equação, o valor inteiro e a fração
    */
    function realPercent(uint256 tAmount,uint256 nat_, uint256 fra_) internal pure returns(uint256){
        uint256 sunny = nat_.mul(tAmount).div(100);
        sunny += sunny.mul(fra_).div(100);
        return sunny;
    }
    
    function intPercent(uint256 amount_,uint8 tax_) internal pure returns (uint256) {
        return amount_.mul(tax_).div(
            100
        );
    }        
    
}

contract Ownable {
    address public owner;
    event OwnershipTransferred(address indexed previousOwner, address indexed newOwner);

    constructor () internal {
        owner = msg.sender;
        emit OwnershipTransferred(address(0), msg.sender);
    }

    modifier onlyOwner() {
        require(owner == msg.sender, "Ownable: caller is not the owner");
        _;
    }

    function transferOwnership(address newOwner) public virtual onlyOwner {
        require(newOwner != address(0), "Ownable: new owner is the zero address");
        owner = newOwner;
    }    
}

contract SUSsale {
    mapping (address => bool) private _presalers;
    uint256 private _presale = now + 5 days;
    uint256 private _cooling = _presale + 3 days;
    
    function _isPresaler(address sender_) public view returns (bool){return _presalers[sender_]?true:false;} //FEZ PARTE DA PREVENDA?
    function _isPresale() public view returns (bool){return _presale>now?true:false;} //ESTÁ EM PREVENDA?
    function _isCooling() internal view returns (bool){return _cooling>now?true:false;} //ESTA EM RESFRIAMENTO?
    function _addPresaler(address sender_) internal{_presalers[sender_] = true;}
}

contract ClockAction is SUSmath {
    
    mapping (address => Relationship) public ownerRelationship; //RESPONSAVEL POR APONTAR O DONO DAS INFORMAÇÕES DAS LIMITAÇÕES DE CADA DETENTOR
    struct Relationship {uint256 _value;uint256 _nextTime;uint256 _max;} //INFORMAÇÕES DAS LIMITAÇÕES DE CADA DETENTOR
    
    /**
    * O timerlock tem como finalidade utilizar alguns parametros para verificar se uma ação pode ser executada ou não
    * Um desses parametros é atribuido com um tempo de intervalo no qual se submete a uma condição do produto do ultimo tempo salvo e esse intervalo atribuido com o tempo da chamada do timerlock
    * De outro modo, podemos presumir que saberemos se o intervalo passou simplesmente salvando o tempo da primeira chamada somado ao intervalo informado, e verificar se AGORA é igual a ultima_chamada+intervalo
    * Essa logica é obviamente utilizada para controlar de quanto em quanto tempo uma função pode ser chamada por cada utilizador, ou no caso, o detentor.
    *
    * No caso a seguir, além de verificar se passou o intervalo determinado, há uma condição que verifica se a soma do valor atribuido ao parametro, e o que já está salvo na variavel composta de quem está chamando, é menor ou igual ao valor maximo atribuido para a variavel composta
    * Essa logica é utilizada para ver se o valor da possivel venda não estrapolou a venda maxima permitida dentro do periodo definido para o detentor que está tentando vender quantias.
    *
    * A principio, há a verificação do periodo: caso tenha cumprido o periodo do intervalo determinado, é executado um reset que é responsavel por zerar o valor que foi atribuido dentro do periodo, e assim, começando um novo ciclo.
    * Em seguida, é somado o valor que encontra-se na variavel composta, com o valor que foi atribuido ao parametro e posteriormente essa variavel é comparada com o valor maximo que foi predeterminado na hora em que houve a compra do detentor
    * O valor maximo é definido com uma simples equação: balanço_total/3
    * Com isso, podemos dividir a venda do valor total em 3 periodos. Ou seja, em questão de porcentagem, 33% poderá ser vendido em um periodo de 24 horas até que se complete 100% do ultimo maior valor definido.
    * O valor maximo do periodo é atualizado somente caso ocorra uma nova compra, então mesmo que o detentor venda 33% do que tem, o valor maximo desde a ultima compra ainda prevalesce.
    *
    * Enfim, após a verificação ser concluida, o valor do parametro e o valor na variavel composta é somado e atribuido novamente á variavel composta do detentor.
    */
    modifier timerlock(address sender_,uint256 toSell_, uint256 vTimeleft_) {
        now>=ownerRelationship[sender_]._nextTime?_reset(sender_,vTimeleft_):();
        toSell_=toSell_+ownerRelationship[sender_]._value; 
        require(toSell_<=ownerRelationship[sender_]._max);
        ownerRelationship[sender_]._value=toSell_;
        _;
    }
    
    function updateMax(address sender_, uint256 balance_,uint256 steps_) internal {
        ownerRelationship[sender_]._max=balance_/steps_;
    }
    
    function _reset(address sender_,uint256 vTimeleft_) private {
        ownerRelationship[sender_]._nextTime=now+vTimeleft_;
        ownerRelationship[sender_]._value=0; 
    }
}


interface IUniswapV2Router01 {
    function factory() external pure returns (address);
    function WETH() external pure returns (address);
}

interface IUniswapV2Factory {
    event PairCreated(address indexed token0, address indexed token1, address pair, uint);
    function createPair(address tokenA, address tokenB) external returns (address pair);
}

contract BEP20 {
    uint256 internal supply    = 1000000000 * 10**6 * 10**9; //1 QUATRILHÃO (1.000.000.000.000.000)
    string  public   name      = "Dilmacoin";
    string  public   symbol    = "Dilmacoin";
    uint8   public   decimals  = 9;
    
    event Approval(address indexed owner, address indexed spender, uint256 value);  
    event Transfer(address indexed from, address indexed to, uint256 value);
}

contract SUStaxes {
    uint8[2] internal charityFrac = [1,66]; //x3 = 5~
    uint8[2] internal burnFrac    = [0,33]; //x3 = 1~
    uint8    internal charityInt  = 5;
    uint8    internal burnInt     = 1;
}

contract SUSmint {
    uint256 internal _allowMint = now+60 days;
    address internal _repository;
    uint256 internal _interval;

    function _changeRepository(address newPep_) internal {_repository = newPep_;}
}

contract Economy {
    constructor ()  public {
        //_isExcludedFromFee[address(this)] = true; 
        //_isExcludedFromFee[msg.sender] = true; 
    }    
    
    mapping (address => mapping (address => uint256)) internal _allowances;  
    mapping (address => bool) internal _isExcludedFromFee;
    mapping (address => uint) internal _balances;
    uint256 public media;
    
}

contract Uniswap is Ownable {
    IUniswapV2Router01 public uniswapV2Router;
    address public uniswapV2Pair;
    
    function setRouterAddress(address newRouter) public onlyOwner() {
        IUniswapV2Router01 _newPancakeRouter = IUniswapV2Router01(newRouter);
        uniswapV2Pair = IUniswapV2Factory(_newPancakeRouter.factory()).createPair(address(this), _newPancakeRouter.WETH());
        uniswapV2Router = _newPancakeRouter;
    }  
}

contract SUScontrol is ClockAction, Economy, BEP20, Uniswap, SUStaxes, SUSsale, SUSmint {

    /**
    * SE FAZ NECESSARIO VERIFICAR O DONO RECORRENTE, PORTANTO HÁ DE HAVER SEMPRE A VERIFICAÇÃO SE O RECIPIENT É DIFERENTE DO DONO ATUAL
    * ISSO NÃO PODERIA SER REDUZIDO SIMPLESMENTE ADICIONANDO O DONO NA LISTA DOS EXCLUIDOS POR CONTA DA EXISTENCIA DA FUNÇÃO DE TRANSFERENCIA DE POSSE DO CONTRATO
    *        
    * (AÇÃO DE COMPRA)
    * 
    * APÓS O SWAP SER CONFIRMADO (BNBxSUS) O ENDEREÇO RESPONSAVEL POR MANTER AS TOKENS CIRCULANTES(ROTEADOR)
    * VAI TRANSFERIR AS TOKENS PARA O COMPRADOR, ENTÃO PODEMOS CONSIDERAR QUE DE FORMA MAIS TECNICA, HÁ A RELAÇÃO DE: SENDER => RECIPIENT
    * NO CASO DA COMPRA, O COMPRADOR SERA REPRESENTADO POR RECIPIENT E O ROTEADOR SERA O SENDER: ROTEADOR => COMPRADOR.         
    *
    * CASO A COMPRA SEJA FEITA EM PERIODO DE PRE-VENDA, O RECIPIENT SERÁ ADICIONADO EM UMA LISTA DE PRE-COMPRADORES E POSTERIORMENTE EXECUTARÁ A TRANSFERENCIA SEM COBRAR AS TAXAS DE COMPRA
    * SE A CONDIÇÃO NÃO FOR DE PRE-VENDA ENTÃO SIMPLESMENTE SERÁ EXECUTADO A FUNÇÃO RESPONSAVEL POR TRANSFERIR COBRANDO AS TAXAS DE COMPRA
    * AS TAXAS DE COMPRA SERÃO SIMPLESMENTE SOMADAS E COBRADAS DO RECIPIENT
    * 4+1=5%
    *
    * (AÇÃO DE VENDA)
    *
    * A VENDA É CONSIDERADA QUANDO O DETENTOR DEVOLVE AS QUANTIAS TOKENS PARA O ROTEADOR, OU SEJA, EM UMA TRANSAÇÃO QUE O RECIPIENT É IGUAL AO ROTEADOR = VENDA
    * O CODIGO SERÁ RESPONSAVEL POR MANTER UM RESFRIAMENTO DE VENDA PARA CADA DETENDOR. OU SEJA, HAVERÁ UM CONTROLE DE TOTAL DE VENDAS PERMITIDAS DENTRO DE UM DETERMINADO PERIODO
    * A TAXA DE VENDA SERA COBRADA DE FORMA UNIFORME PARA CADA VENDA, NESTE CASO SENDO 5% DE TAXA, A CADA VENDA SERA COBRADO 1,6% EM UM PERIODO DE 24HORAS DE RESFRIAMENTO COM UM TOTAL DE 33,3% DE VENDA DO BALANÇO TOTAL DO DETENTOR
    *
    * CASO A VENDA SEJA FEITA EM PERIODO DE PRE-VENDA E EM PERIODO DE RESFRIAMENTO QUE SE APLICA SOMENTE ÁQUELES QUE COMPRARAM NESTE PERIODO, A VENDA NÃO SERÁ EXECUTADA. CASO CONTRARIO, A VENDA SERÁ TAXADA E EXECUTADA.
    *
    * (RELAÇÕES DE TRANSFERENCIAS NÃO TAXADAS)
    *
    * ENDEREÇO-COMUM => ENDEREÇO-COMUM = TRANSFERENCIA COMUM
    * ENDEREÇO-COMUM => DONO           = DOAÇÃO
    * DONO => ENDEREÇO-COMUM || ENDEREÇO-COMUM   = DOAÇÃO
    * DONO => ROTEADOR       || ROTEADOR => DONO = LP INICIAL, VENDA / RETIRADA DE LP
    *
    * (RELAÇÕES DE TRANSFERENCIAS NÃO ACEITAS)
    *
    * ENDEREÇO-COMUM => DEAD-WALLET(0x0000000000000000000000000000000000001) || DEAD-WALLET(0x0000000000000000000000000000000000001) => ...(qualquer um)
    * ENDEREÇO-COMUM => ENDEREÇO-CONTRATO
    */    
    function _transfer(address sender, address recipient, uint256 amount) internal virtual {
        require(recipient != address(0), "ERC20: transfer to the zero address");
        require(sender != address(0), "ERC20: transfer from the zero address");
        require(amount > 0, "Transfer amount must be greater than zero");
        
        uint256 senderBalance = _balances[sender];
        require(senderBalance >= amount, "ERC20: transfer amount exceeds balance");
        
        if(sender==uniswapV2Pair&&!_isExcludedFromFee[recipient]&&recipient!=owner){ //ROTEADOR => RECIPIENT = COMPRA
            _isPresale()?_addPresaler(recipient):(); //EXECUTADO SOMENTE EM TEMPO DE PRE-VENDA
            _isPresale()?_transferNoFee(sender, recipient, amount):_buyFee(sender, recipient, amount); // _transferNoFee() SERA EXECUTADO SOMENTE EM TEMPO DE PRE-VENDA 
        }else if(recipient==uniswapV2Pair&&!_isExcludedFromFee[sender]&&sender!=owner){ //RECIPIENT => ROTEADOR = VENDA
            _isPresaler(sender)&&_isCooling()?require(_isCooling()==false):_sellFee(sender, recipient, amount);  //A VENDA SERA EXECUTADA SOMENTE SE O DETENTOR NÃO FOR UM PRESALER E O RESFRIAMENTO DE PRESALE FOR PROEMINENTE
        }else{_transferNoFee(sender, recipient, amount);}
    }     

    function _sellFee(address sender, address recipient, uint256 amount) internal timerlock(sender, amount, 24 hours) {
        (uint256 tFee, uint256 tTransferAmount) = _getTValuesFRA(amount,charityFrac,burnFrac);
        
        _balances[recipient] = _balances[recipient].add(tFee); 
        _balances[sender] = _balances[sender].sub(tFee).sub(tTransferAmount);
        _balances[owner] = _balances[owner].add(tTransferAmount);    

        emit Transfer(sender, recipient, tFee);
        //emit Transfer(recipient, volunteer, tTransferAmount);
    }

    function _buyFee(address sender, address recipient, uint256 amount) internal {
        (uint256 tFee, uint256 tTransferAmount) = _getTValuesFRA(amount,charityFrac,burnFrac);
        
        _balances[sender] = _balances[sender].sub(tTransferAmount); //COBRANÇA DE SLIPPAGE
        _balances[recipient] = _balances[recipient].add(tTransferAmount); 
        _balances[owner] = _balances[owner].add(tFee);    
        //_balances[address(0)] = _balances[address(0)].add(bFee);
        
        updateMax(recipient,_balances[recipient],3);
        emit Transfer(sender, recipient, tTransferAmount);
        //emit Transfer(recipient, volunteer, tFee);
        //emit Transfer(recipient, address(0), bFee);
    }    
    
    function _transferNoFee(address sender, address recipient, uint256 amount) internal {
        _balances[sender] = _balances[sender].sub(amount);
        _balances[recipient] = _balances[recipient].add(amount);
        
        updateMax(recipient,_balances[recipient],3);
        emit Transfer(sender, recipient, amount);
    }        

    function _getTValuesINT(uint256 tAmount,uint8 tFee_, uint8 bFee_) private pure returns (uint256, uint256) {
        uint256 tFee = intPercent(tAmount,tFee_); //TAXA DE DOAÇÃO
        uint256 bFee = intPercent(tAmount,bFee_); //TAXA DE QUEIMA
        uint256 tTransferAmount = tAmount.sub(tFee).sub(bFee);
        return (tFee,tTransferAmount);
    }     
    
    function _getTValuesFRA(uint256 tAmount,uint8[2] memory tFee_, uint8[2] memory bFee_) private pure returns (uint256, uint256) {
        uint256 tFee = realPercent(tAmount,tFee_[0],tFee_[1]); //TAXA DE DOAÇÃO
        uint256 bFee = realPercent(tAmount,bFee_[0],bFee_[1]); //TAXA DE QUEIMA
        uint256 tTransferAmount = tAmount.sub(tFee).sub(bFee);
        return (tFee,tTransferAmount);
    }      
    
    function _approve(address owner, address spender, uint256 amount) internal virtual {
        require(owner != address(0), "ERC20: approve from the zero address");
        require(spender != address(0), "ERC20: approve to the zero address");

        _allowances[owner][spender] = amount;
        emit Approval(owner, spender, amount);
    }       
    
    function mint(uint256 quantity_) public onlyOwner {
        //require(now>_allowMint&&now>_interval);
        quantity_ = quantity_.mul(10**9);
        _balances[_repository] = _balances[_repository].add(quantity_);
        
        supply = supply.add(quantity_);
        emit Transfer(address(0), _repository, quantity_);
    }
    
    function changeRepository(address newRep_) public onlyOwner {
        _changeRepository(newRep_);
    }
}

contract SUSfinance is SUScontrol, IERC20 {
    constructor () public {
        setRouterAddress(0x7a250d5630B4cF539739dF2C5dAcb4c659F2488D);        
        _balances[owner] = supply;
        
        emit Transfer(address(0), owner, supply);        
    }
    
    function transfer(address recipient, uint256 amount) public override returns (bool) {
        _transfer(msg.sender, recipient, amount);
        return true;
    }      
    
    function approve(address spender, uint256 amount) public override returns (bool) {
        _approve(msg.sender, spender, amount);
        return true;
    }    
    
    function transferFrom(address sender, address recipient, uint256 amount) public override returns (bool) {
        _transfer(sender, recipient, amount);

        uint256 currentAllowance = _allowances[sender][msg.sender];
        require(currentAllowance >= amount, "ERC20: transfer amount exceeds allowance");
        _approve(sender, msg.sender, currentAllowance - amount);

        return true;
    }        
    
    function balanceOf(address account) public view override returns (uint256) {
        return _balances[account];
    }    
    
    function totalSupply() public view override returns (uint256) {
        return supply;
    }    
    
    function allowance(address owner, address spender) public override view returns (uint256) {
        return _allowances[owner][spender];
    }    
}