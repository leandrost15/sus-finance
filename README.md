**Ideologia**

Somos influenciados pela diferença dentro do ecossistema no geral, afim de destacar nossos valores e qualidades, que
sobretudo destacamos a capacidade de inovar, visando não somente na ancianidade do projeto em si, mas de forma que
possamos equidistar a concorrencia e sairmos do cenario em que todo projeto é sujeitado a seguir roteiros
com a necessidade de competitividade entre pontos iguais assim como comercios fiduciario.
Consideramos a qualidade da inovação uma chave para novos projetos incomuns, isso se aplica 
devido a necessidade de competitividade de pontos diferentes, pois a longevidade no mercado exige o diferencial para
vencer a concorrencia. No cenario recorrente, acreditamos somos a chave para abrir novas possibilidades de projetos 
visionarios.

Podemos considerar a necessidade revolucionaria como a mãe da humanidade, porém uma ideia oportuna de sapiência e
heterogeneidade resume-se a um assunto delicado devido claramente a subjetividade. Portanto, agregamos a entrega
de resultados nessa questão, desta forma, suprimos as duvidas quanto a viabilidade com os resultados alcançados 
como forma de demonstrar interesse e competencia.

A cultura deste concentra-se na proliferação intuitiva e imersiva entre a realidade e o mercado de ponta, dando vida
a um novo ecossistema financeiro, possibilitando outras formas de ganhos além do sistema fiduciario padrão onde todos
são submetidos a um sistema de ganhos em troca de um serviço ou produto, no que resume o mercado de trabaho 
recorrente.
Uma vez substancial, a principio, não haverá o devido valor moral na ausencia de um valor de troca fora do escopo do
mercado de ponta. A intersecção dos ecossistemas permitirá a possibilidade da convergência do virtual para o fisico, seja
um produto ou um serviço, o processo que é conhecido por Omnichannel.

**Introdução**

Desde o inicio da atual pandemia, a importancia da economia criptografica vem sendo afamada devido as necessidades 
fundamentais, tais como o proprio isolamento de cada ser humano afim de combater a disseminação do virus.
Infelizmente, houve uma alta contingente de 33,1% de desempregados dentro do periodo de estado de calamidade.
Segundo a coordenadora da pesquisa, Maria Lúcia Vieira, a alta no desemprego é explicada pelo maior número de 
pessoas voltando a procurar emprego diante da flexibilização do isolamento social pelo país.

(Fonte: g1.globo.com)

Por mais que o isolamento social seja de fato a melhor opção para nos manter seguros da doença, ainda há suas 
outras enfermidades, tais como a depressão, sedentarismo, obesidade e até mesmo o infarte. Caracterizando uma das
piores doenças, a depressão é um transtorno de humor marcado pela sensação persistente de tristeza, perca de
interesse em quaisquer atividades e ate mesmo no reforço ou desinteresse no apetite, o que de fato pode acarretar
o desenvolvimento dos outros enfermos citados. A depressão pode ser desenvolvida através de um colapso mental 
acionado por algum acontecimento traumatizante ou modelo bruto de tentativas, falhas e desistencias que podem ou
não ser reprimidas. A correlação deste enfermo com o desemprego é um fator brutal por privar as possibilidades que
o ser humano tem para suprir suas necessidades fisiologicas e manter a segurança seus familiares.

Devido á abundancia tecnologica que temos hoje em dia, torna-se possivel adaptar o mundo real com o ecossistema irreal
cripto financeiro para criar um novo tipo de economia afim de sobrepujar os problemas de um ecossistema com o outro
e desta forma criando um envolucro para reforçar o isolamento social. Problemas como os enfermos citados acimas podem
ser resolvidos com o modelo de ação baseada em recompensa, quase identico ao modelo recorrente do mercado de trabalho.
A solução da problematica principal deste projeto visa ampliar e adaptar a tecnologia de forma submersa á area da saude
com os principios da inovação, afim de criar e desenvolver a qualidade de vida de cada ser humano de forma complementar
e altruista.

Neste projeto, temos como objetivo auxiliar a sociedade com modelos versateis de remuneração de forma sucinta á historia
pandemica da atualidade e aperfeiçoamento subito com uma economia criptografica, buscando embasamento e novas 
concorrencias no mercado de cripto financeiro, apresentando um projeto inovador e convidativo. Entretanto de forma 
comunitaria, que expõe a sociedade a um novo ecossistema real e irreal.

(mapa mental aqui )

Estes são alguns exemplos hipoteticos, descrevendo o conceito da relação com o ecossistema do projeto com projetos 
subsidiados. Ambos são submetidos a um escopo padrão, sendo eles: Tema, ações/produtos correlacionados ao token, e
o ecossistema monetário que descreve os mecanismos de utilidade do token.

**Case de sucesso:**

Roberto é um desenvolvedor de aplicativos mobile e também um investidor de criptotokens e criptomoedas. Roberto que está
acostumado a espelhar seus projetos em outros já existente, sente a necessidade de inovar e destacar suas aplicações.
Então, Roberto começa a refletir uma maneira de seus usuários receberem recompensas monetarias atráves das conquistas
que seus usuários adquirem. Uma vez idealizado seu projeto, Roberto parte em busca de parcerias mas infelizmente não
há sucesso devido a falta de reciprocidade onde seus parceiros não tem retorno de investimento. Com mais pesquisas
Roberto conhece a SUS Finance, que de fato tem o mesmo objetivo que o de Roberto, criar modelos de recompensas.

Louis é jovem de 23 anos solteiro, trabalha 8 horas por dia com um salario minimo mensal e também um aspirante á atleta.
Em suas horas livres, Louis gosta de ir para a academia onde se submete a correr 10km em todos os seus dias de treino.
Um certo dia, Louis é proibido de trabalhar pelo seu chefe pela razão do isolamento e distanciamento que estava ausente
em seu ambiente de trabalho. Sem saber como prosseguir sua vida sem emprego em isolamento, e estar limitado a continuar
com suas periodicas corridas e treinos, Louis passa a sobreviver com suas economias. Logo, passa a desenvolver o
sedentarismo, e por suas economias estarem acabando, começa a refletir muito em como prosseguir do ponto em que esta
e passa a desenvolver a depressão. Roberto que é amigo de Louis, percebe uma brusca mudança de humor do seu amigo. Logo
Roberto oferece uma inusitada solução para os problemas de Louis: Um aplicativo que promete comprar a quilometragem que
o Louis pode adquirir correndo pelas ruas, sendo pago em quantias criptotokens. Criptotokens que podem ser convertidas
em moedas fiduciarias de sua origem, ou até mesmo no revolucionario mercado que também aceita tais criptotokens como
forma de pagamento.

**Filantropia**

A sapiência surge no terceiro nivel da hierarquia das necessidades humanas, ou como conhecemos: A Piramide de Maslow. Em vista disso 
sabemos que a necessidade fisiologica vem como terreo tanto da hierarquia quanto das preocupações de cada ser 
humano, sendo assim, aderimos o altruismo com o tema da saude, tecnologia e inovação.

(piramide de maslow imagem aqui)

O recurso arrecadado será mediante ao crescimento do projeto SUS Finance e o metodo da arrecadação será através da
absorção de cada transação, seja de venda ou seja de compra. Isto é, a participação neste projeto conta com doações
obrigatorias. Gostariamos de deixar bem claro que este não é o foco principal do projeto, portanto, reforçamos a 
oposição sobre gatilhos mentais e emocionais e sugerimos que se submeta a ler todo o documento afim de entender nossa
ideia central.

As doações serão destinadas para fins medicos e farmaceuticos, com a finalidade de formar um novo acesso a 
produtos e serviços na area da saude e garantir o mesmo para aqueles que encontram-se em um estado financeiro 
decadente. Quanto ao metodo de doação: A SUS Finance tem em paralelo, recolher, submeter e expor a sociedade á esse
novo ecossistema financeiro. Por mais que o estado financeiro recorrente seja critico, ainda é importante compartilhar
o conhecimento para que em um futuro prospero, o donatário faça parte dessa intersecção e experimente novos caminhos,
no que diz respeito á Piramide de Maslow.

O donatário precisará passar por verificações rigidas antes que a doação seja concluida:

    * Fornecer uma prova de identidade (RG, Passaporte ou Carteira de Motorista)
    * Comprovante de endereço registrado em seu nome (Contas gerais de energia ou água, 
    confirmação do banco, contrato de aluguel ou identificação do governo)
    * Receita medica, informando de forma detalhada no formulario de solicitação
    * Video explicando de forma detalhada qual o problema de saude em questão

Todas as doações serão análisadas, documentadas, condicionalmente executadas e por fim o documento da doação contendo
inclusive o hash da transação(TX) e seu video, será postado no blog do site.

A execução das doações serão feitas pela equipe, de forma minusciosa e controlada semanalmente para não haver um colapso
de mercado. Assim sendo, uma quantidade limitada de doações serão feitas semanalmente. A quantidade sera baseada no
volume de mercado recorrente.

**Segurança**

Recentemente, o ramo de DeFi atingiu mais de R$ 500 bilhões em ativos alocados. Para Igamberdiev, isso é mais do 
que suficiente para atrair pessoas interessadas em “abocanhar o bolo”. Bem-afamado conceitualmente, o DeFI esbanja
flexibilidade e somado com o crescimento da rede, muitos projetos querem "surfar na onda" e criam projetos ás
pressas, muitas vezes tais projetos são baseados em hard forks, isso significa que o projeto foi baseado em outro
projeto. No ramo DeFI, não há problema algum em "forkar" um projeto, na verdade é a melhor opção desde que o projeto
siga os padroes da rede tais como ERC20 e BEP20 por oferecer um codigo completamente seguro, mas em oposição, varios
forks são feito ás pressas, sem uma análise bem estruturada e detalhada consequentemente carregando consigo problemas
de segurança e algumas violações do padrão ERC20

(Fonte: criptofacil)

O smart-contract deste projeto foi minuciosamente programado, violando alguns padrões ERC20 de forma consciente, e com alguns 
poderes disponibilizados apenas para a equipe de forma segura e controlada com a finalidade de executar todas as projeções 
mencionadas neste documento. Isto significa que as visiveis falhas neste projeto estão de acordo com o desenvolvimento 
sugerido desde o principio.

Podemos começar com a visivel possibilidade de executar o mais famoso golpe do DeFI: O RugPull, que é quando a 
equipe por trás de um projeto liquida seus tokens e some com o dinheiro. A solução para este problema pode ser 
implementada basicamente controlando as relações de transferencias de tokens no proprio smart-contract, onde o
roteador devolve/transfere/envia suas quantias de volta para o endereço que tem a posse do contrato. Aliás, sabe-se
que o roteador é o endereço responsavel por trocar as quantias tokens por um valor injetado na liquidez ou seja, quando
há uma compra, o roteador será responsavel por enviar as quantias tokens para o comprador, e quando houver uma venda
o receptor do processo sera o roteador, visto como o processo de liquidez. Em outras palavras, a solução para essa
falha seria proibir que o roteador envie tokens para o endereço dono do contrato de forma direta.
    
    * ENDEREÇO-COMUM => ENDEREÇO-COMUM                = TRANSFERENCIA COMUM
    * ENDEREÇO-COMUM => DONO / DONO => ENDEREÇO-COMUM = DOAÇÃO
    * ENDEREÇO-COMUM => ROTEADOR                      = VENDA
    * ROTEADOR       => ENDEREÇO-COMUM                = COMPRA
    * ROTEADOR       => DONO                          = RETIRADA DE LP
    * DONO           => ROTEADOR                      = ADIÇÃO DE LIQUIDEZ / VENDA DE QUANTIAS ARRECADADAS
(transformar isso em uma tabela)

O posicionamento deste projeto, conclui que devido a longevidade, futuras mudanças, melhorias e correções podem ser
vistas, aliás DeFi ainda é um conceito novo mas pode haver grandes falhas com o decorrer do tempo, que vão acompanhar
a longevidade deste projeto e desta forma, é nosso dever pensar tanto no presente como no futuro. Desta forma, caso
necessite de uma nova atualização em nosso contrato, será necessario um hard fork para um novo contrato, e se faz
necessario a disponibilidade de controlar a liquidez contida em seu mercado para que desta forma seja movimentado
de forma segura, e que seja feito a redistribuição das quantias tokens dos detentores recorrente.
No entanto, como apresentação do projeto, a relação entre o roteador e o possuidor do contrato, será bloqueado por
um periodo de 2 meses.

No entanto, ainda mesmo com uma visivel solução para o visivel RugPull, ainda há a possibilidade de criar novas
quantias tokens por haver inflação ao alimentar o ecossistema de recompensa, o que pode ser similado a um possivel 
RugPull se não for minusciosamente controlado. Contudo, foi estabelecido que a inflação será estudada antes de ser
executada para não haver possiveis colapsos de mercado. Este periodo de estudo da equipe será mediado pelo bloqueio
a cada inflação que for executada, sendo possivel executar uma vez a cada trinta dias. O periodo também funciona
como um mecanismo de segurança para que neste periodo seja executado os devidos protocolos para a segurança interna
ou avaliação de equipe costumeira.

Quanto as questões de premiações e recompensas, salienta-se que o processo passará por avaliações manuais e até mesmo
incluindo dApps desenvolvidos pela propria equipe para agilizar o processo de forma correta e segura, podendo ser 
incluido a necessidade do recurso de verificação em KYC tanto manual como automatica, isso dependerá de cada projeto.

A politica de segurança deste projeto, concentra-se principalmente em não utilizar dApps suspeitos ou desnecessarios,
portanto tudo o que pudermos faremos atraves de nosso smart-contract de forma segura, justa e correto. Com este intuito,
resolvemos criar uma nova carteira para limpar o historico de acesso dos dApps

